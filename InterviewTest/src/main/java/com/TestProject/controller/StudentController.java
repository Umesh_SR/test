package com.TestProject.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.TestProject.entity.CommonResponse;
import com.TestProject.entity.Student;
import com.TestProject.repository.StudentReopsitory;


@RestController
@RequestMapping("/Student")
public class StudentController {
	
	
	@Autowired
	private StudentReopsitory repository;
	
	
	@PostMapping("/addStudent")
	private ResponseEntity<CommonResponse> saveStudent(@RequestBody Student student) {
		CommonResponse UR = new CommonResponse();

		repository.insert(student);
		UR.setData(student);
		return new ResponseEntity<CommonResponse>(UR, UR.getStatus());
	}

	
	@PostMapping("/updateStudent")
	private ResponseEntity<CommonResponse> updateStudent(@RequestBody Student student) {
		CommonResponse UR = new CommonResponse();
		repository.save(student);
		UR.setData(student);
		return new ResponseEntity<CommonResponse>(UR, UR.getStatus());
	}
	
	@GetMapping("/findAllStudent")
	private List<Student> findAllStudent() {
		return repository.findAll();
	}


}
