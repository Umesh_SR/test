package com.TestProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class InterviewDetailsApplication {

	public static void main(String[] args) {
		SpringApplication.run(InterviewDetailsApplication.class, args);
	}

}
