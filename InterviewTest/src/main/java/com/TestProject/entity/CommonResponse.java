package com.TestProject.entity;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class CommonResponse {

	
	
	private HttpStatus Status=HttpStatus.OK;
	private String message="Success";
	private Object Data;
}
