package com.TestProject.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Document(collection = "Student")
public class Student {
	
	@Id
	private String id;
	private String firstName;
	private String lastName;
	

}
